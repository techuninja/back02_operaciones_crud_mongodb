package com.techu.productosdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductosdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductosdbApplication.class, args);
    }

}
