package com.techu.productosdb.repository;

import com.techu.productosdb.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {


}
